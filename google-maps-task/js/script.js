// API key : AIzaSyBS8Mj4MjP033EEC3hdwDLcRgNX-B9b3x0
var mapIcon = 'images/marker.png';
var markers = [];
var userExists;
var statuses = {
  'ZERO_RESULTS': 'Address cannot be displayed because its not found!',
  'OVER_QUERY_LIMIT': 'Please wait before placing a new marker'
}

// Custom method for jQuery validator to restrict special characters
jQuery.validator.addMethod("restrictCharacters", function(value, element) {
  return this.optional(element) || /^[a-z0-9\ \s]+$/i.test(value);
}, "Name must only contain letters, numbers or spaces");

/**
 * Function creates map, geocoder and infowindow objects
 * validateInput and formSubmit is called 
 * Click listener for adding a marker is attached to the map 
 */
function initMap() {
  var initialLocation = {
    lat: 42.698334,
    lng: 23.319941
  };
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: initialLocation
  });
  var geocoder = new google.maps.Geocoder;
  var infowindow = new google.maps.InfoWindow;

  autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'), {
    types: ['geocode']
  });
  validateInputs();
  formSubmit(map, geocoder);
  
  map.addListener('click', function (event) {
    cleanMarkers();
    geocodeLatLng(geocoder, map, event.latLng)
    addMarker(event.latLng, map);
    setMapOnAll(map);
  });
}

/**
 * Creates new marker on the map and push it in array
 * @param  {Object} location holds latitude and longitude values
 * @param  {Object} map parsed Google map object
 */
function addMarker(location, map) {
  var marker = new google.maps.Marker({
    position: location,
    map: map,
    zoom: 20,
    icon: mapIcon
  });
  markers.push(marker);
}

/**
 * Places markers in the array on the map
 * @param  {Object} map
 */
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

/**
 * Clears placed marker if there is one,
 * thus preventing having more than one marker on the map
 */
function cleanMarkers() {
  if (markers.length > 0) {
    setMapOnAll(null);
    markers = [];
  }
}

/**
 * Function autocompletes address input by using Google API
 * @param  {Objects} resultsMap parsed Google map object
 * @param  {Object} geocoder parsed Google map geocoder object
 */
function geocodeAddress(resultsMap, geocoder) {
  if (userExists) {
    return false;
  }
  address = document.getElementById('autocomplete').value;
  geocoder.geocode({
    'address': address
  }, function (results, status) {
    if (status === 'OK') {
      resultsMap.setCenter(results[0].geometry.location);
      addMarker(results[0].geometry.location, resultsMap);
      resultsMap.setZoom(15);
    } else {
      window.alert(statusValidate(status));
    }
  });
}

/**
 * Function converts latitude and longitude in readable address
 * Places readable adress in address input
 * @param  {Object} geocoder parsed Google map geocoder object
 * @param  {Object} map parsed Google map object
 * @param  {Object} eventLatLng holding latitude and longitude values
 */
function geocodeLatLng(geocoder, map, eventLatLng) {
  var input = String(eventLatLng);
  var latlngStr = input.slice(1).slice(0, -1).split(',', 2);
  var latlng = {
    lat: parseFloat(latlngStr[0]),
    lng: parseFloat(latlngStr[1])
  };

  geocoder.geocode({
    'location': latlng
  }, function (results, status) {
    if (status === 'OK') {
      if (results[0]) {
        addMarker(latlng, map);
        $('#autocomplete').val(results[0].formatted_address);
      } else {
        window.alert('No results found');
      }
    } else {
      window.alert(statusValidate(status));
    }
  });
}

/**
 * Submits the form after submit button is clicked
 * Check if the form is valid, if not displays error message
 * @param  {Object} map parsed Google map object
 * @param  {Object} geocoder parsed Google map geocoder object
 */
function formSubmit(map, geocoder) {
  $('#buttonSubmit').on('click', function (event) {
    if ($('#myForm').valid()) {
      cleanMarkers();
      saveUser();
      geocodeAddress(map, geocoder);
      notifyUser();
    } else {
        $('span.success').html('Please enter valid data!').css('color', 'red');
    }
  });
}

/**
 * Uses jQuery validate plugin to set rules and validate the form
 * Displays error on focus out
 */
function validateInputs() {
  $('#myForm').validate({
    rules: {
      name: {
        required: true,
        minlength: 3,
        restrictCharacters: true
      },
      address: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      phone: {
        required: true,
        number: true,
        minlength: 4,
        maxlength: 15
      },
      url: {
        required: true,
        url: true
      }
    },
    onfocusout: function (element) {
      this.element(element);
    }
  });
}

/**
 * Creates new user with data from the inputs
 */
function saveUser() {
  var userName = $('input[name=name]').val();
  var userAddress = $('input[name=address]').val();
  var userEmail = $('input[name=email]').val();
  var userPhone = $('input[name=phone]').val();
  var userUrl = $('input[name=url]').val();
  var newUser = new User(userName, userAddress, userEmail, userPhone, userUrl);

  checkUsers(newUser);
}

class User {
  constructor(name, address, email, phone, url) {
    this.userName = name;
    this.userAddress = address;
    this.userEmail = email;
    this.userPhone = phone;
    this.userUrl = url;
  }
}

/**
 * Puts new user in local storage
 * Prevents creating new user if there is user with existing email
 * @param  {Object} newUser created user holding data
 */
function checkUsers(newUser) {
  var users;
  var userLength;
  userExists = false;
  if (localStorage.getItem('users') === null) {
    users = [];
    users.push(newUser);
    localStorage.setItem('users', JSON.stringify(users));
  } else {
    users = localStorage.getItem('users');
    users = JSON.parse(users);
    for (var i = 0, userLength = users.length; i < userLength; i++) {
      if (users[i].userEmail == newUser.userEmail) {
        userExists = true;
        break;
      }
    }
    if (!userExists) {
      users.push(newUser);
      localStorage.setItem('users', JSON.stringify(users));
    }
  }
}

/**
 * Notifies user upon success/error submission
 */
function notifyUser() {
  if (userExists) {
    $('span.success').html('');
    $('span.error-email').html('Email already in use!');
  } else {
    $('span.error-email').html('');
    $('span.success').html('Data saved!').css('color', '#00b894');
  }
}

/**
 * Translate Google API errors in human language
 * @param  {String} status message 
 */
function statusValidate(status) {
  if (status == ZERO_RESULTS) {
      status = statuses[status];
      return status;
  } else if (status == OVER_QUERY_LIMIT) {
      status = statuses[status];
      return status;
  }
}