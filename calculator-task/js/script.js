// Get all the keys from document
var keys = $('#calculator span');
var decimalAdded = false;
var calculated = false;
var input = $('.screen');
input.text('0');
var maxLength = 28;
var badInput = ['-Infinity', 'Infinity', 'NaN', 'undefined'];

var numberRegEx = new RegExp("[\\d]");
var operatorsRegEx = new RegExp("[\\+\\-\\*\\/]");
var specialRegEx = new RegExp("[\\.\\=]");
var lettersRegEx = new RegExp("[A-Z]");

$(document).ready(function() {
	initFn();
});

function initFn() {
	keyWatch();
	listenKeys();
}

 // Listen for keys in keyboard
function listenKeys() {
	$(window).on('keypress', function(event){
	   // Convert char codes into string
	   var chr = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	   if(numberRegEx.test(chr) || operatorsRegEx.test(chr) || specialRegEx.test(chr)) {
		   var inputVal = input.text();
		   resetInput(chr, inputVal);
		   buttonCases(chr, inputVal);
	   }
	}).on('keydown', function(e){
		var inputVal = input.text();
		// Check if clicked key have keyCode 8 = Backspace
		if (e.keyCode == 8) {
			var inputVal = input.text();
			if (inputVal.length > 1) {
				if (inputVal[inputVal.length - 1] == '.') {
					decimalAdded = false;
				}
				inputVal = inputVal.slice(0, -1);
				input.text(inputVal);	
			} else {
				input.text('0');
			}
		// Check if clicked key have keyCode 13 = Enter
		} else if (e.keyCode == '13') {
			input.text(evalEquation(inputVal));
			calculated = true;
		}
	});
}

// Listen for keys in document
function keyWatch() {
	$(keys).on('click', function() {
		var btnVal = $(this).text();
		var inputVal = input.text();
		resetInput(btnVal, inputVal);
		buttonCases(btnVal, inputVal);
	});
}

/**
 * Evaluates input
 * Handles error messages
 * @param  {string} equation
 * @returns {string} Processed equation as string
 */
function evalEquation(equation) {
	try {
		var lastChar = equation[equation.length - 1];
		if (operatorsRegEx.test(lastChar)) {
			equation = equation.slice(0, -1);
		} 

		equation = String(eval(equation));
		if (badInput.indexOf(equation) > -1) {
			equation = 'Cannot divide by zero!';
		}
		decimalAdded = false;
		return equation;
		
	}
	catch (err) {
		input.text('Bad Input!');
		calculated = true;
	}
}

/**
 * Checks if input have initial value = 0 and changes the 0 to a new entered digit
 * Disables entering more than one 0 before floating point
 * Appends pressed button in the end of the string
 * @param  {string} btn button that is pressed
 */
function appendInput(btn) {
	var inputVal = input.text();
	var lastChar = inputVal[inputVal.length - 1];
	var preLastChar = inputVal[inputVal.length - 2];
	if (inputVal == '0' && numberRegEx.test(btn)) {
		inputVal = btn;
		input.text(inputVal);
	} else if (operatorsRegEx.test(preLastChar) && lastChar == '0') {
		if (numberRegEx.test(btn)) {
		inputVal = inputVal.slice(0, -1);
		}
		inputVal += btn;
		input.text(inputVal);
	} else {
		inputVal += btn;
		input.text(inputVal);
	}

}

/**
 * Replaces the last character in inputVal string if its operator with the new entered operator
 * Flags decimalAdded as false after operator is entered
 * @param  {string} keySymbol button that is pressed 
 * @param  {string} inputVal current input string
 */
function validateInput(keySymbol, inputVal){
	if (operatorsRegEx.test(keySymbol)) {
		var lastChar = inputVal[inputVal.length - 1];
		if (operatorsRegEx.test(lastChar) || lastChar == '.') {
			inputVal = inputVal.slice(0, -1);
			inputVal += keySymbol;
			input.text(inputVal);
		} else {
			appendInput(keySymbol);
		}
		decimalAdded = false;

	} else {
		appendInput(keySymbol);
	}
}

/**
 * Function triggers after button is pressed and clears input if there is error messages
 * If current pressed button is operator enable adding it after equation is calculated
 * If input have error messages or equation is calculated, clear input and flag off calculated and decimalAdded
 * @param  {string} chr button that is pressed
 * @param  {string} inputVal current input string
 */
function resetInput(chr, inputVal) {
	console.log(lettersRegEx.test(inputVal));
	if (operatorsRegEx.test(chr) && calculated) {
		calculated = false;
	}

	if (calculated || lettersRegEx.test(inputVal)) {
		input.text('0');
		calculated = false;
		decimalAdded = false;
	 }
}

/**
 * Different case processing for any of the special keys/buttons pressed
 * @param  {string} btnVal buttons that is pressed
 * @param  {string} inputVal current input string
 */
function buttonCases (btnVal, inputVal) {
	var cases = {
		'.': function () {
			if (!decimalAdded) {
				var lastChar = inputVal[inputVal.length - 1];
				if (operatorsRegEx.test(lastChar) || inputVal == '0') {
					btnVal = '0' + btnVal;
				}
				appendInput(btnVal);
				decimalAdded = true;
			}
		},
		'CE': function () {
			if (inputVal.length > 1) {
				if (inputVal[inputVal.length - 1] == '.') {
					decimalAdded = false;
				}
				inputVal = inputVal.slice(0, -1);
				input.text(inputVal);	
			} else {
				input.text('0');
			}
		},
		'C': function () {
			input.text('0');
		},
		'=': function () {
			input.text(evalEquation(inputVal));
			calculated = true;
		},
		'default': function () {
			var inputVal = input.text();
			validateInput(btnVal, inputVal);
		}
	};
	
	(cases[btnVal] || cases['default'])();
}